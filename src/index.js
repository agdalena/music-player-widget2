import "../scss/main.scss";

const LOADING_TIME = 1000;
const playBtn = document.querySelector("#btn-play");
const nextBtn = document.querySelector("#btn-next");
const prevBtn = document.querySelector("#btn-prev");
const load = document.getElementById("load");
const songsTab = ["Parostatkiem w piękny re.js", "Mój przyjacielu", "Bo jesteś Ty"];
const titleSong = document.querySelector("#p-title");
const progressBar = document.querySelector(".progress-bar");

let progressInterval;
let songIndex = 0;
let progress = 0;

playBtn.addEventListener("click", changeIcon);
nextBtn.addEventListener("click", changeSongsNext);
prevBtn.addEventListener("click", changeSongsPrev);

function changeIcon() {
        playBtn.classList.toggle("fa-pause");  
        if(progressInterval) {
                stopProgress();
            } else {
                startProgress();
            }
        }

function loading() {
        load.classList.toggle("d-none");
        }
 

function changeSongsNext() {
        progress = 0;
        loading();
        setTimeout(loading, LOADING_TIME);
        songIndex++;
        if(songIndex === 3){;
            songIndex = 0;
        }
        titleSong.innerHTML = songsTab[songIndex];

}

function changeSongsPrev() {
        loading();
        setTimeout(loading, LOADING_TIME);
        songIndex--;
        if(songIndex === -1){
            songIndex = 2;
        }
        titleSong.innerHTML = songsTab[songIndex];
}

function stopProgress() {
        if(progressInterval) {
            clearInterval(progressInterval);
            progressInterval = undefined;
        }
}

function startProgress() {
        progressInterval = setInterval(updateProgress, 100);
}

function updateProgress() {
        progress++;
        progressBar.style.width = progress + "%";
        if(progress >= 100) {
            clearInterval(progressInterval);
            progressInterval = undefined;
        }
}
